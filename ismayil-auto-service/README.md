# Ismayil-Car-Service 

Car backend-service

## General info

This backend-service developed to practice my knowledge on Java Spring. I have successfully managed to write APIs for Car service


### Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Docker](https://www.docker.com/products/docker-desktop/)
- [Gradle](https://gradle.org/install/)


### Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.autoservice.ismayil.IsmayilAutoServiceApplication` class from your IDE.

But first of all you need to run :
```shell
cd project-folder
docker-compose up
```

### Contribution
This project isn't open for contributions

### Authors
* **Ismayil Mohsumov**
