package com.autoservice.ismayilautoservice.controller;

import com.autoservice.ismayilautoservice.common.GenericSearchDto;
import com.autoservice.ismayilautoservice.dto.SellerRequest;
import com.autoservice.ismayilautoservice.dto.SellerResponse;
import com.autoservice.ismayilautoservice.service.SellerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sellers")
@RequiredArgsConstructor
public class SellerController {
    private final SellerService sellerService;

    @PostMapping
    public SellerResponse save(@RequestBody SellerRequest request) {
        return sellerService.save(request);
    }

    @GetMapping("/{id}")
    public SellerResponse get(@PathVariable int id) {
        return sellerService.get(id);
    }

    @GetMapping
    public List<SellerResponse> getAll() {
        return sellerService.getAll();
    }

    @PostMapping("/search")
    public Page<SellerResponse> search(@RequestBody GenericSearchDto searchDto,
                                       @PageableDefault Pageable pageable) {
        return sellerService.search(searchDto, pageable);
    }

    @PutMapping("/{id}")
    public SellerResponse update(@PathVariable int id,
                                 @RequestBody SellerRequest sellerRequest) {
        return sellerService.update(id, sellerRequest);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        sellerService.delete(id);
    }

}
