package com.autoservice.ismayilautoservice.controller;

import com.autoservice.ismayilautoservice.common.GenericSearchDto;
import com.autoservice.ismayilautoservice.dto.AutoRequest;
import com.autoservice.ismayilautoservice.dto.AutoResponse;
import com.autoservice.ismayilautoservice.service.AutoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/autos")
public class AutoController {

    private final AutoService autoService;

    @PostMapping
    public AutoResponse save(@RequestBody @Valid AutoRequest request) {
        log.info("Creating new auto {}", request);
        return autoService.save(request);
    }

    @GetMapping("/{id}")
    public AutoResponse get(@PathVariable int id) {
        return autoService.get(id);
    }


    @GetMapping
    public List<AutoResponse> getAll() {
        return autoService.getAll();
    }

    @PutMapping("/{id}")
    public AutoResponse update(@PathVariable int id,
                               @RequestBody AutoRequest request) {
        return autoService.update(id, request);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        autoService.delete(id);
    }

    @PostMapping("/search")
    public Page<AutoResponse> search(@RequestBody GenericSearchDto genericSearchDto,
                                     @PageableDefault Pageable pageable) {
        return autoService.search(genericSearchDto, pageable);
    }

}
