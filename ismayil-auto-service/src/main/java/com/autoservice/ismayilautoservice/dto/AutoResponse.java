package com.autoservice.ismayilautoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AutoResponse {

    private Integer id;

    private String vin;

    private String make;

    private String model;

    private Integer year;

    private Integer milage;

    private String vehicleType;

    private String color;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

}

