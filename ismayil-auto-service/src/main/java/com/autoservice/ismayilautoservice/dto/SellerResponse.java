package com.autoservice.ismayilautoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellerResponse {

    private Integer id;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private List<AutoResponse> autos;

}
