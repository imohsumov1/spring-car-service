package com.autoservice.ismayilautoservice.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AutoRequest {


    @NotBlank(message = "VIN must not be empty")
    @Size(min = 17, max = 17, message = "Vin must be 17 simbol")
    private String vin;

    @NotBlank(message = "Brand must not be empty")
    private String make;

    @NotBlank(message = "Model must not be empty")
    private String model;

    @Min(value = 1950, message = "Production year must not be empty")
    private Integer year;

    @Min(value = 0, message = "Mileage must not be empty")
    private Integer milage;

    private String vehicleType;

    private String color;

    @NotNull(message = "Seller must not be empty")
    private Integer sellerId;


}
