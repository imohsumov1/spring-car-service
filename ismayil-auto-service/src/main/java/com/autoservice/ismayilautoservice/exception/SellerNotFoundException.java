package com.autoservice.ismayilautoservice.exception;

public class SellerNotFoundException extends RuntimeException {

    private static final String MESSAGE = "Seller not found by given id (id : %d)";

    public SellerNotFoundException(Integer id) {
        super(String.format(MESSAGE, id));
    }
}
