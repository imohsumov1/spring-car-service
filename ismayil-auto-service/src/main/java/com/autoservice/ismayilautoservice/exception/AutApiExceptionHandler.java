package com.autoservice.ismayilautoservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestControllerAdvice
public class AutApiExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Map<String, String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<Map<String, String>> errors = new ArrayList<>();
        e.getFieldErrors().forEach(fieldError -> errors.add(Map.of(fieldError.getField(), Objects.requireNonNull(fieldError.getDefaultMessage()))));
        return errors;
    }

    @ExceptionHandler(AutoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleAutoNotFoundException(AutoNotFoundException e) {
        return e.getMessage();
    }

    @ExceptionHandler(SellerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleSellerNotFoundException(SellerNotFoundException e) {
        return e.getMessage();
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e) {
        return e.getMessage();
    }

}
