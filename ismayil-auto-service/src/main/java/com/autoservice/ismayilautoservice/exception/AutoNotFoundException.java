package com.autoservice.ismayilautoservice.exception;

public class AutoNotFoundException extends RuntimeException {

    private static final String MESSAGE = "The Car not found by given id (id : %d)";

    public AutoNotFoundException(Integer id) {
        super(String.format(MESSAGE, id));
    }
}
