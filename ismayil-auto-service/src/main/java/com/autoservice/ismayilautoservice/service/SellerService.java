package com.autoservice.ismayilautoservice.service;

import com.autoservice.ismayilautoservice.common.GenericSearchDto;
import com.autoservice.ismayilautoservice.common.search.SearchSpecification;
import com.autoservice.ismayilautoservice.dto.SellerRequest;
import com.autoservice.ismayilautoservice.dto.SellerResponse;
import com.autoservice.ismayilautoservice.entity.Seller;
import com.autoservice.ismayilautoservice.exception.SellerNotFoundException;
import com.autoservice.ismayilautoservice.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SellerService {

    private final SellerRepository sellerRepository;
    private final ModelMapper modelMapper;

    public SellerResponse save(SellerRequest sellerRequest) {
        return modelMapper.map(sellerRepository
                .save(modelMapper
                        .map(sellerRequest, Seller.class)), SellerResponse.class);
    }

    public SellerResponse get(int id) {
        return sellerRepository.findById(id)
                .map(seller -> modelMapper.map(seller, SellerResponse.class))
                .orElseThrow(() -> new SellerNotFoundException(id));
    }

    public List<SellerResponse> getAll() {
        return sellerRepository.findAll()
                .stream().map(seller -> modelMapper.map(seller, SellerResponse.class))
                .collect(Collectors.toList());
    }

    public Page<SellerResponse> search(GenericSearchDto searchDto, Pageable pageable) {
        return sellerRepository
                .findAll(new SearchSpecification<>(searchDto.getCriteria()), pageable)
                .map(seller -> modelMapper.map(seller, SellerResponse.class));
    }

    public SellerResponse update(int id, SellerRequest sellerRequest) {
        return sellerRepository.findById(id)
                .map(seller -> {
                    seller = modelMapper.map(sellerRequest, Seller.class);
                    seller.setId(id);
                    return modelMapper.map(sellerRepository.save(seller), SellerResponse.class);
                }).orElseThrow(() -> new SellerNotFoundException(id));
    }


    public void delete(int id) {
        checkSellerExists(id);
        sellerRepository.deleteById(id);
    }

    void checkSellerExists(int id) {
        if (!sellerRepository.existsById(id)) throw new SellerNotFoundException(id);
    }

}
