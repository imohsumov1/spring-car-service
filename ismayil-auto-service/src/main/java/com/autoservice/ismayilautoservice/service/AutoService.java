package com.autoservice.ismayilautoservice.service;

import com.autoservice.ismayilautoservice.common.GenericSearchDto;
import com.autoservice.ismayilautoservice.common.search.SearchSpecification;
import com.autoservice.ismayilautoservice.dto.AutoRequest;
import com.autoservice.ismayilautoservice.dto.AutoResponse;
import com.autoservice.ismayilautoservice.entity.Auto;
import com.autoservice.ismayilautoservice.entity.Seller;
import com.autoservice.ismayilautoservice.exception.AutoNotFoundException;
import com.autoservice.ismayilautoservice.exception.SellerNotFoundException;
import com.autoservice.ismayilautoservice.repository.AutoRepository;
import com.autoservice.ismayilautoservice.repository.SellerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AutoService {

    private final ModelMapper modelMapper;
    private final AutoRepository autoRepository;
    private final SellerRepository sellerRepository;

    public AutoResponse save(AutoRequest request) {
        Auto auto = (modelMapper.map(request, Auto.class));
        checkSellerExists(request.getSellerId());
        auto.setSeller(Seller.builder().id(request.getSellerId()).build());
        auto = autoRepository.save(auto);
        return modelMapper.map(auto, AutoResponse.class);
    }

    public AutoResponse get(Integer id) {
        return autoRepository.findById(id)
                .map(auto -> modelMapper.map(auto, AutoResponse.class))
                .orElseThrow(() -> new AutoNotFoundException(id));
    }

    public List<AutoResponse> getAll() {
        return autoRepository.findAll().stream()
                .map(auto -> modelMapper.map(auto, AutoResponse.class)).collect(Collectors.toList());
    }

    public AutoResponse update(Integer id, AutoRequest request) {
        return autoRepository.findById(id)
                .map(auto -> {
                    auto = modelMapper.map(request, Auto.class);
                    checkSellerExists(request.getSellerId());
                    auto.setSeller(Seller.builder().id(request.getSellerId()).build());
                    auto.setId(id);
                    return modelMapper.map(autoRepository.save(auto), AutoResponse.class);
                }).orElseThrow(() -> new AutoNotFoundException(id));
    }

    public void delete(int id) {
        checkIfAutoExists(id);
        autoRepository.deleteById(id);
    }


    public Page<AutoResponse> search(GenericSearchDto genericSearchDto, Pageable pageable) {

        System.out.println("generic = " + genericSearchDto);
        System.out.println("pageable = " + pageable);
        return autoRepository.findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(auto -> modelMapper.map(auto, AutoResponse.class));

    }

    void checkIfAutoExists(Integer id) {
        if (id == null || !autoRepository.existsById(id))
            throw new AutoNotFoundException(id);
    }

    void checkSellerExists(Integer id) {
        if (id == null || !sellerRepository.existsById(id))
            throw new SellerNotFoundException(id);
    }


}
