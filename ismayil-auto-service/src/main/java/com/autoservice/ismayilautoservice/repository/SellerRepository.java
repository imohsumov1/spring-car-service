package com.autoservice.ismayilautoservice.repository;

import com.autoservice.ismayilautoservice.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SellerRepository extends JpaRepository<Seller, Integer>, JpaSpecificationExecutor<Seller> {

}
