package com.autoservice.ismayilautoservice.repository;

import com.autoservice.ismayilautoservice.entity.Auto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AutoRepository extends JpaRepository<Auto, Integer>, JpaSpecificationExecutor<Auto> {
}
